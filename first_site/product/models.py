from django.db import models

# Create your models here.
class Product(models.Model):
   name = models.CharField(max_length=256)
   category=models.CharField(max_length=256)
   
class Category(models.Model):
    