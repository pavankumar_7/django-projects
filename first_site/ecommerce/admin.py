from django.contrib import admin
from .models import Product, Category, CartItem
# Register your models here.

admin.site.register([Product, Category, CartItem])
