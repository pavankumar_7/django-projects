from django.urls import path, include
from . import views
from django.contrib.auth import views as auth_views

app_name='shop'

urlpatterns = [
    path('home', views.product_list, name='home'),
    path('cart/add/<int:product_id>', views.add_to_cart, name='add_cart'),
    path('cart', views.view_cart, name='cart'),
    path('login', auth_views.LoginView.as_view(template_name='ecommerce/login.html'), name='login'),
    path('logout', auth_views.LogoutView.as_view(), name='logout')
]
