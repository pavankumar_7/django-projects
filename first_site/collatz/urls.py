from django.urls import path
from . import views
urlpatterns = [
    path('series/<int:limit>', views.collatz_sequence, name='collatz_sequence'),    
    path('series/form', views.collatz_sequence_form, name='collatz_sequence-form'),
    path('hello', views.say_hello, name='hello'),
    path('series/rot13',views.rot13,name='rot13-form'),
    path('random',views.random_arithmetic_operation,name="random")
]
