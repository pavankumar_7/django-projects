from django.shortcuts import render,redirect
from django.http import HttpResponse
import random
# Create your views here.


def collatz(number):
    lst=[]
    lst.append(number)
    while(number!=1):
        if(number%2==0):
            number=number//2
            lst.append(number)
        else:
            number=number*3+1
            lst.append(number)
    return lst

def collatz_sequence(request,limit):
    series = collatz(limit) 
    return HttpResponse(f'''
    <!DOCTYPE html>
    <html>
        <body>
            <h1>Collatz Sequence</h1>
            <p>{series}</p>
        </body>
    </html>
    ''')
    
def collatz_sequence_form(request):
    print(type(request))
    if request.method == 'GET':
        return render(request,'collatz/collatz_form.html')
    if request.method == 'POST':
        limit = int(request.POST.get('limit'))
        # return redirect('collatz_sequence',limit)
        series = collatz(limit) 
        return HttpResponse(f'''
        <!DOCTYPE html>
        <html>
            <body>
                <h1>Collatz Sequence</h1>
                <p>{series}</p>
            </body>
        </html>
        ''')
        
def say_hello(request):
    name = request.GET.get('name', 'World')
    num_beers = int(request.GET.get('beers', 100))
    context = {'name': name, 'num_beers':num_beers}
    return render(request, 'collatz/hello.html', context)

def rot13_form(text):
    result = ""
    for char in text:
        if char.isalpha():
            shift = 13
            if char.isupper():
                result += chr((ord(char) - 65 + shift) % 26 + 65)
            else:
                result += chr((ord(char) - 97 + shift) % 26 + 97)
        else:
            result += char
    return result

def rot13(request):
    context={}
    if request.method == 'GET':
        return render(request,'collatz/rot13.html')
    if request.method == 'POST':
        original_text = request.POST.get('text', '')
        converted_text = rot13_form(request.POST.get('text', ''))
        context['converted_text'] = converted_text
        context['original_text'] = original_text
        return render(request,'collatz/rot13.html',context)

# def rot13(request):
#     context = {}
#     if request.method == 'POST':
#         alphas = 'abcdefghijklmnopqrstuvwxyzabcdefghijklm'
#         original_text = request.POST.get('text', '')
#         converted_text = ''.join([alphas[alphas.index(c) + 13] for c in original_text])
#         #converted_text = rot13convert(request.POST.get('text', ''))
#         context['converted_text'] = converted_text
#         context['original_text'] = original_text
#     return render(request,'collatz/rot13.html', context)

def perform_operation(num1, num2, operation):
    if operation == "+":
        return num1 + num2
    elif operation == "-":
        return num1 - num2
    elif operation == "*":
        return num1 * num2
    else:
        return num1 / num2

def random_arithmetic_operation(request):
    num1 = random.randint(1, 1000)
    num2 = random.randint(1, 1000)
    operations = ["+", "-", "*", "/"]
    operation = random.choice(operations)
    result = perform_operation(num1, num2, operation)
    context={
        'num1': num1,
        'num2': num2,
        'operation': operation,
        'result': result,
    }
    
    return render(request, 'collatz/random.html', context)




