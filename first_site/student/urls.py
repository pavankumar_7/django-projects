from django.urls import path
from . import views
urlpatterns = [
    path('', views.show_search, name='show_search'),
    path('search', views.search, name='search'),
    path('<int:id>', views.student_detail, name='student_detail'),
    path('all', views.student_list, name='student_list')
]
