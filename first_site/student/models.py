from django.db import models

# Create your models here.

class Student (models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return f'Student: {self.name}'

class Mark (models.Model):
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    name = models.CharField(max_length=256)
    score = models.DecimalField(max_digits=5, decimal_places=2)

    def __str__(self):
        return f'{self.student}: {self.name} - {self.score}'
