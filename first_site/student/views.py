from django.shortcuts import render, redirect
from .models import Student, Mark

# Create your views here.

def show_search(request):
    return render(request, 'student/search_form.html')

def search(request):
    id_ = request.POST.get('id')
    return redirect('student_detail', id_)

def student_detail(request, id):
    student = Student.objects.get(id=id)
    return render(request, 'student/student_detail.html', {'student': student})

def student_list(request):
    students = Student.objects.all()
    return render(request, 'student/student_list.html', {'students': students})
