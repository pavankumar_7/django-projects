from django.shortcuts import render
from decimal import Decimal

# Dictionary containing menu items and prices
MENU = {
    "Pizza": {
        "Pizza": Decimal('350'),
    },
    "Coffee": {
        "Coffee": Decimal('600'),
    }
}

def index(request):
    if request.method == 'POST':
        pizza = request.POST.get('pizza')
        coffee = request.POST.get('coffee')
        pizza_price = MENU['Pizza'][pizza] if pizza else 0
        coffee_price = MENU['Coffee'][coffee] if coffee else 0
        total = pizza_price + coffee_price
        context = {
            'pizza': pizza,
            'coffee': coffee,
            'pizza_price': pizza_price,
            'coffee_price': coffee_price,
            'total': total
        }
        return render(request, 'menu/bill.html', context)
    else:
        context = {
            'pizza_choices': MENU['Pizza'],
            'coffee_choices': MENU['Coffee']
        }
        return render(request, 'menu/index.html', context)
