from django.shortcuts import render

# Create your views here.
from django.shortcuts import render, redirect
from .models import Menu, Bill
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required, permission_required
@login_required(login_url = 'login')    
def menu(request):
    menu = Menu.objects.all()
    return render(request, 'order/index.html', {'menu': menu})

def login_(request):
    if request.method == 'GET':
        return render(request, 'order/loginpage.html')
    if request.method == 'POST':
        username_ = request.POST.get('username', '')
        entered_password = request.POST.get('password', '')
        user = authenticate(username=username_, password=entered_password)
        print(user)
        if user is not None:
            login(request, user)
        return redirect('total_menu')
    
@login_required(login_url='login')
def logout_(request):
    logout(request)
    return redirect('login')

@login_required(login_url= 'login')
def order(request, id):
    items = Menu.objects.get(id = id)
    try:
        bill = Bill.objects.get(order = items,user=request.user)
    except Bill.DoesNotExist:
        Bill.objects.create(order = items,user=request.user)
    bill, created = Bill.objects.get_or_create(order = items, user=request.user)
    bill.quantity = bill.quantity + 1
    bill.subtotal = bill.quantity * bill.order.cost
    bill.save()
    billing = Bill.objects.filter(user = request.user)
    total = sum([item.subtotal for item in billing])
    gst = (total * 3)//100
    grandtotal = total + gst
    return render(request,'order/bill.html', {'billing': billing, 'total' : total, 'gst': gst, 'grandtotal' : grandtotal})
def increment(request,id):
    quantity = Bill.objects.get(id = id)
    quantity.quantity += 1
    quantity.subtotal = quantity.quantity * quantity.order.cost
    quantity.save()
    billing = Bill.objects.filter(user = request.user)
    total = sum([item.subtotal for item in billing])
    gst = (total * 3)//100
    grandtotal = total + gst
    return render(request,'order/bill.html', {'billing': billing, 'total' : total, 'gst': gst, 'grandtotal' : grandtotal})

def decrement(request,id):
    quantity = Bill.objects.get(id = id)
    if quantity.quantity > 1:
        quantity.quantity -= 1
        quantity.subtotal = quantity.quantity * quantity.order.cost
        quantity.save()
        billing = Bill.objects.filter(user = request.user)
        total = sum([item.subtotal for item in billing])
        gst = (total * 3)//100
        grandtotal = total + gst
        return render(request,'order/bill.html', {'billing': billing, 'total' : total, 'gst': gst, 'grandtotal' : grandtotal})
    elif quantity.quantity == 1 : 
        quantity.delete()
        billing = Bill.objects.filter(user = request.user)
        total = sum([item.subtotal for item in billing])
        gst = (total * 3)//100
        grandtotal = total + gst
        return render(request,'order/bill.html', {'billing': billing, 'total' : total, 'gst': gst, 'grandtotal' : grandtotal})