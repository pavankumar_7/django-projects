from django.db import models
from django.contrib.auth.models import User


class Menu(models.Model):
    img = models.URLField(max_length=256, null=True)
    name = models.CharField(max_length=256)
    cost = models.DecimalField(max_digits=10, decimal_places=2)

    def _str_(self):
        return f'{self.img}- {self.name} - {self.cost}'

class Bill(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    order = models.ForeignKey(Menu, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)
    subtotal = models.DecimalField(max_digits=10, decimal_places=2, default=0)