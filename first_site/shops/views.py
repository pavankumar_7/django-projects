from django.shortcuts import render, redirect
from .models import Products
from django.contrib.auth.decorators import permission_required
from django.contrib.auth import authenticate, login, logout
from.models import  Products,Category,CartItem

# Create your views here.

# def product_detail(request, id):
#     product = Products.objects.get(id = id)
#     return render(request, 'eapp/product_detail.html', {'product': product})
def products_list(request):
    products = Products.objects.all()
    return render(request, 'shops/temp.html', {'products': products})

def login_(request):
    if request.method == 'GET':
        return render(request, 'shops/authenticate.html')
    if request.method == 'POST':
        username_ = request.POST.get('username', '')
        entered_password = request.POST.get('password', '')
        user = authenticate(username=username_, password=entered_password)
        print(user)
        if user is not None:
            login(request, user)
        return redirect('products_list')

def logout_(request):
    logout(request)
    return redirect('shops:login_')

@permission_required('shops.add_cartitem', login_url='shops:login')
def add_to_cart(request, product_id):
    product_ = Products.objects.get(pk=product_id)
    try:
        cartitem = CartItem.objects.get(product=product_, user=request.user)
    except CartItem.DoesNotExist:
        CartItem.objects.create(
            product=product_,
            user=request.user)
    cartitem, created = CartItem.objects.get_or_create(product=product_, user=request.user)
    cartitem.quantity += 1
    cartitem.save()
    return redirect('shops:cart')

@permission_required('shops.view_cartitem',login_url='shops:login')
def view_cart(request):
    cartitems = CartItem.objects.filter(user=request.user)
    return render(request, 'shops/cart.html', {'cartitems': cartitems})
