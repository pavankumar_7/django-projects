from django.urls import path
from . import views
# from django.contrib.auth import views as auth_views

app_name='shops'

urlpatterns = [
    # path('<int:id>', views.product_detail, name='product_detail'),
    path('', views.products_list, name='products_list'),
    path('login', views.login_, name='login'),
    path('logout', views.logout_, name='logout'),
    path('cart',views.view_cart,name='cart'),
    path('cart/add/<int:product_id>',views.add_to_cart,name='add_cart')

]