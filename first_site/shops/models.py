from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Products(models.Model):
    img = models.URLField(max_length=256, null=True)
    name = models.CharField(max_length=256)
    cost = models.DecimalField(max_digits=10, decimal_places=2)

    def _str_(self):
        return f'{self.img}: {self.name} - {self.cost}'
    
class CartItem(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Products, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0)
    
class Category(models.Model):
    name = models.CharField(max_length=256)

    def __str__(self):
        return self.name
  
  
